import 'package:cartoons_flutter/model/character.dart';

final characters = [
  Character(
    name: 'Albert Alemany',
    age: 28,
    image: 'images/albert.png',
    jobTitle: 'Carpintero',
    stars: 4.3,
  ),
  Character(
    name: 'Gerard Guasch',
    age: 21,
    image: 'images/gerard.png',
    jobTitle: 'Mecánico',
    stars: 3.8,
  ),
  Character(
    name: 'Ignasi Isern',
    age: 33,
    image: 'images/ignasi.png',
    jobTitle: 'Cerrajera',
    stars: 4.9,
  ),
  Character(
    name: 'Meritxell Maimó',
    age: 29,
    image: 'images/meritxell.png',
    jobTitle: 'Escritora',
    stars: 4.1,
  ),
  Character(
    name: 'Mònica Moragues',
    age: 24,
    image: 'images/monica.png',
    jobTitle: 'Peluquera',
    stars: 3.5,
  ),
  Character(
    name: 'Pol Pitarch',
    age: 19,
    image: 'images/pol.png',
    jobTitle: 'Soldador',
    stars: 2.9,
  ),
  Character(
    name: 'Raquel Reixach',
    age: 35,
    image: 'images/raquel.png',
    jobTitle: 'Administradora',
    stars: 3.8,
  ),
  Character(
    name: 'Rebeca Roig',
    age: 31,
    image: 'images/rebeca.png',
    jobTitle: 'Abogada',
    stars: 4.6,
  ),
  Character(
    name: 'Ricard Ricart',
    age: 22,
    image: 'images/ricard.png',
    jobTitle: 'Matemático',
    stars: 4.0,
  ),
  Character(
    name: 'Sílvia Salom',
    age: 27,
    image: 'images/silvia.png',
    jobTitle: 'Artesana',
    stars: 3.9,
  ),
];
